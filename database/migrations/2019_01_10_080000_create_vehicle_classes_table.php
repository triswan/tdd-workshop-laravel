<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleClassesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_classes',
            function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 191)->unique();
            $table->string('name');
            $table->string('description');
//            $table->timestamps();
        });

        DB::table('vehicle_classes')->insert([
            [
                'id' => 1,
                'code' => '001',
                'name' => 'Motor',
                'description' => 'Motor',
            ],
            [
                'id' => 2,
                'code' => '002',
                'name' => 'Kendaraan Kecil',
                'description' => 'Kendaraan Kecil, contoh: sedan, suv',
            ],
            [
                'id' => 3,
                'code' => '003',
                'name' => 'Kendaraan Sedang',
                'description' => 'Kendaraan Sedang, contoh: minibus',
            ],
            [
                'id' => 4,
                'code' => '004',
                'name' => 'Kendaraan Besar',
                'description' => 'Kendaraan Besar, contoh: truk container',
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_classes');
    }
}