<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnforcementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enforcements', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('passage_id')->unsigned();
            $table->integer('type_id');
            $table->integer('fine');
            $table->integer('status_id');
            $table->timestamps();
            $table->foreign('passage_id')->references('id')->on('passages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enforcements');
    }
}
