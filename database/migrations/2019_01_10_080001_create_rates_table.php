<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lane');
            $table->integer('vehicle_class_id')->unsigned()->nullable();
            $table->integer('account_type_id')->nullable();
            $table->datetime('start_time')->nullable();
            $table->datetime('end_time')->nullable();
            $table->integer('price');
            $table->timestamps();
            $table->foreign('vehicle_class_id')->references('id')->on('vehicle_classes');
        });
        DB::table('rates')->insert([
            [
                'id' => 1,
                'lane' => 1,
                'vehicle_class_id' => 1,
                'price' => 10000,
            ],
            [
                'id' => 2,
                'lane' => 2,
                'vehicle_class_id' => 2,
                'price' => 25000,
            ],
            [
                'id' => 3,
                'lane' => 1,
                'vehicle_class_id' => 3,
                'price' => 30000,
            ],
            [
                'id' => 4,
                'lane' => 1,
                'vehicle_class_id' => 4,
                'price' => 50000,
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rates');
    }
}
