<?php

use Faker\Generator as Faker;

$factory->define(App\Account::class, function (Faker $faker) {
    $plate = strtoupper($faker->randomLetter).$faker->numberBetween(1111, 9999).strtoupper($faker->randomLetter);

    return [
        'number' => $faker->bankAccountNumber,
        'vehicle_class_id' => $faker->numberBetween(1,4),
        'license_plate' => $plate,
        'balance' => $faker->randomNumber(5),
        'status_id' => $faker->numberBetween(1,2),
    ];
});