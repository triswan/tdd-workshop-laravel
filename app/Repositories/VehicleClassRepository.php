<?php

namespace App\Repositories;

use App\Repositories\VehicleClassRepositoryInterface;
use App\VehicleClass;

class VehicleClassRepository implements VehicleClassRepositoryInterface
{

    function getByCode($code)
    {
        return VehicleClass::where('code', $code)->first();
    }
}