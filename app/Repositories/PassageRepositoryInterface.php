<?php

namespace App\Repositories;

interface PassageRepositoryInterface
{

    function create($passage);
}