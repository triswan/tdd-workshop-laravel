<?php

namespace App\Repositories;

interface AccountRepositoryInterface
{
    const STATUS_ACTIVE   = 1;
    const STATUS_INACTIVE = 2;

    function getActiveByNumber($number);
}