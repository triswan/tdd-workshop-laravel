<?php

namespace App\Repositories;

interface VehicleClassRepositoryInterface
{

    function getByCode($code);
}