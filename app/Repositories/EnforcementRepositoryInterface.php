<?php

namespace App\Repositories;

interface EnforcementRepositoryInterface
{
    const STATUS_BELUM_DIBAYAR = 1;
    const STATUS_SUDAH_DIBAYAR = 2;

    function create($enforcement);

}