<?php

namespace App\Repositories;

use App\Repositories\RateRepositoryInterface;
use App\Rate;

class RateRepository implements RateRepositoryInterface
{

    function all()
    {
        return Rate::all();
    }

    function create($rates)
    {
        return Rate::create($rates);
    }
}