<?php

namespace App\Repositories;

use App\Repositories\EnforcementRepositoryInterface;
use App\Enforcement;

class EnforcementRepository implements EnforcementRepositoryInterface
{

    function create($enforcement)
    {
        Enforcement::create($enforcement);
    }

}