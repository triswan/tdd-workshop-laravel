<?php

namespace App\Repositories;

use App\Repositories\PassageRepositoryInterface;
use App\Passage;

class PassageRepository implements PassageRepositoryInterface
{

    function create($passage)
    {
        return Passage::create($passage);
    }
}