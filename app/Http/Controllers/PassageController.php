<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Account;
use App\Passage;
use App\VehicleClass;

class PassageController extends Controller
{

    function receive(Request $request)
    {
        $ps = new Passage();

        $ac = Account::where('status_id', 1)->where('number', $request->account_number)->first();

        if ($ac != '') {
            $vc = VehicleClass::where('code', $request->vehicle_class)->first();
            if ($vc != '') {
                $vcId = $vc->id;
            } else {
                $vcId = null;
            }

            if ($vcId == 1) {
                $harga = 10000;
            } else if ($vcId == 2) {
                $harga = 25000;
            } else if ($vcId == 3) {
                $harga = 30000;
            } else if ($vcId == 4) {
                $harga = 50000;
            }

            $ps->price            = $harga;
            $ps->account_id       = $ac->id;
            $ps->vehicle_class_id = $vcId;
            $ps->license_plate    = $request->license_plate_number;
            $ps->lane             = $request->lane;
            $ps->save();

            if ($ac->balance < $harga) {
                DB::table('enforcements')->insert(['passage_id' => $ps->id, 'type_id' => 1, 'fine' => 50000, 'status_id' => 1]); // WARNING: hardcode & magic numbers
            }
            $ac->balance = $ac->balance - $harga;
            $ac->save();

            echo 'success';
        } else {     // Handle requirement pelanggaran tanpa IKE
            $vc = VehicleClass::where('code', $request->vehicle_class)->first();
            if ($vc != '') {
                $vcId = $vc->id;
            } else {
                $vcId = null;
            }

            if ($vcId == 1) {
                $harga = 10000;
            } else if ($vcId == 2) {
                $harga = 25000;
            } else if ($vcId == 3) {
                $harga = 30000;
            } else if ($vcId == 4) {
                $harga = 50000;
            }

            $ps->price            = $harga;
            $ps->account_id       = null;
            $ps->vehicle_class_id = $vcId;
            $ps->license_plate    = $request->license_plate_number;
            $ps->lane             = $request->lane;
            $ps->save();


            DB::table('enforcements')->insert(['passage_id' => $ps->id, 'type_id' => 2, 'fine' => 100000, 'status_id' => 1]); // WARNING: hardcode & magic numbers

            echo 'success';
        }
    }
}