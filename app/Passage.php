<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Passage extends Model
{
    protected $fillable = ['account_id', 'vehicle_class_id', 'license_plate', 'lane', 'rate_id', 'price', 'type_id', 'status_id'];
    protected $guarded  = ['id'];

}